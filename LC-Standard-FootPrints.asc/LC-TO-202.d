*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-202        I 0     0     2 2 3 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-204.79 122.24 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-204.79 172.24 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-157.48 0    
-173.23 0    
OPEN   2 8 26 -1
-200.79 90.55
200.79 90.55
CLOSED 5 8 26 -1
-200.79 -70.87
-200.79 110.24
200.79 110.24
200.79 -70.87
-200.79 -70.87
T-100  0     -100  0     1
T0     0     0     0     2
T100   0     100   0     3
PAD 1 3 P 43.31
-2 78.74 S   0  
-1 78.74 S   0  
0  78.74 S   0  
PAD 0 3 P 43.31
-2 78.74 R  
-1 78.74 R  
0  78.74 R  

*END*
