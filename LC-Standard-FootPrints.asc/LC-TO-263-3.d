*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-263-3      I 0     0     2 2 5 0 4 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-590.55 209.85 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-590.55 259.85 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-110.24 -157.48
-125.98 -157.48
OPEN   4 10 26 -1
-153.54 -118.11
-78.74 -118.11
-78.74 -82.68
-153.54 -82.68
OPEN   4 10 26 -1
-153.54 17.32
-94.49 17.32
-94.49 -18.11
-153.54 -18.11
OPEN   4 10 26 -1
-153.54 117.32
-78.74 117.32
-78.74 81.89
-153.54 81.89
OPEN   4 10 26 -1
-181.1 -196.85
-153.54 -196.85
-153.54 196.85
-181.1 196.85
T-393.7 0     -393.7 0     0
T0     -100  0     -100  1
T0     0     0     0     2
T0     100   0     100   3
PAD 1 3 P 0    
-2 393.7 RF  0   90   397.64 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 59.06 RF  0   0    118.11 0  
-1 0   R  
0  0   R  

*END*
