*PADS-LIBRARY-PCB-DECALS-V9*

LC-2512_L        I 0     0     2 2 2 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-162.4 91.07 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-162.4 141.07 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-124.02 62.99
124.02 62.99
124.02 -62.99
-124.02 -62.99
-124.02 62.99
CLOSED 5 7.87 26 -1
-158.46 79.13
158.46 79.13
158.46 -79.13
-158.46 -79.13
-158.46 79.13
T-122.05 0     -122.05 0     1
T122.05 0     122.05 0     2
PAD 0 3 P 0    
-2 49.21 RF  0   90   134.65 0  
-1 0   R  
0  0   R  

*END*
