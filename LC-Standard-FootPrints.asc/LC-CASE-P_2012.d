*PADS-LIBRARY-PCB-DECALS-V9*

LC-CASE-P_2012   I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-90.61 49.5  0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-90.61 99.5  0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-39.37 25.59
39.37 25.59
39.37 -25.59
-39.37 -25.59
-39.37 25.59
OPEN   6 10 26 -1
23.62 36.5 
71.93 36.5 
86.61 24.54
86.61 -24.54
71.93 -36.5
23.62 -36.5
OPEN   4 10 26 -1
-23.62 -36.5
-71.93 -36.5
-71.93 36.5 
-23.62 36.5 
OPEN   2 10 26 -1
71.93 -36.5
71.93 36.5 
OPEN   2 8 19 -1
118.11 -19.68
118.11 19.68
OPEN   2 8 19 -1
98.43 0    
137.8 0    
OPEN   2 8 19 -1
-86.61 -19.68
-86.61 19.68
T35.43 0     35.43 0     1
T-35.43 0     -35.43 0     2
PAD 0 3 P 0    
-2 47.24 RF  0   0    47.24 0  
-1 0   R  
0  0   R  

*END*
