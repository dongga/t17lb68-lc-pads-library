*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOD-723       I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-49.21 33.59 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-49.21 83.59 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-19.68 11.81
19.68 11.81
19.68 -11.81
-19.68 -11.81
-19.68 11.81
OPEN   2 7.87 26 -1
-33.46 -21.65
33.46 -21.65
OPEN   2 7.87 26 -1
-33.46 21.65
33.46 21.65
OPEN   2 7.87 26 -1
-5.91 -21.65
-5.91 21.65
OPEN   2 3.94 19 -1
-47.24 -11.81
-47.24 11.81
OPEN   2 3.94 19 -1
47.24 0    
70.87 0    
OPEN   2 3.94 19 -1
59.06 -11.81
59.06 11.81
T27.56 0     27.56 0     1
T-27.56 0     -27.56 0     2
PAD 0 3 P 0    
-2 19.68 RF  0   0    23.62 0  
-1 0   R  
0  0   R  

*END*
