*PADS-LIBRARY-PCB-DECALS-V9*

LC-VFBGA-48      I 0     0     2 2 3 0 48 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-83.74 91.74 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-83.74 141.74 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-64.96 -102.36
-76.77 -102.36
CLOSED 5 10 26 -1
-78.74 -78.74
-78.74 78.74
78.74 78.74
78.74 -78.74
-78.74 -78.74
OPEN   2 10 26 -1
-78.74 -66.93
-66.93 -78.74
T-59.05 -59.05 -59.05 -59.05 A1
T-59.05 -39.37 -59.05 -39.37 A2
T-59.05 -19.68 -59.05 -19.68 A3
T-59.05 0     -59.05 0     A4
T-59.05 19.69 -59.05 19.69 A5
T-59.05 39.37 -59.05 39.37 A6
T-59.05 59.06 -59.05 59.06 A7
T-39.37 -59.05 -39.37 -59.05 B1
T-39.37 -39.37 -39.37 -39.37 B2
T-39.37 -19.68 -39.37 -19.68 B3
T-39.37 0     -39.37 0     B4
T-39.37 19.69 -39.37 19.69 B5
T-39.37 39.37 -39.37 39.37 B6
T-39.37 59.06 -39.37 59.06 B7
T-19.68 -59.05 -19.68 -59.05 C1
T-19.68 -39.37 -19.68 -39.37 C2
T-19.68 0     -19.68 0     C4
T-19.68 19.69 -19.68 19.69 C5
T-19.68 39.37 -19.68 39.37 C6
T-19.68 59.06 -19.68 59.06 C7
T0     -59.05 0     -59.05 D1
T0     -39.37 0     -39.37 D2
T0     -19.68 0     -19.68 D3
T0     0     0     0     D4
T0     19.69 0     19.69 D5
T0     39.37 0     39.37 D6
T0     59.06 0     59.06 D7
T19.69 -59.05 19.69 -59.05 E1
T19.69 -39.37 19.69 -39.37 E2
T19.69 -19.68 19.69 -19.68 E3
T19.69 0     19.69 0     E4
T19.69 19.69 19.69 19.69 E5
T19.69 39.37 19.69 39.37 E6
T19.69 59.06 19.69 59.06 E7
T39.37 -59.05 39.37 -59.05 F1
T39.37 -39.37 39.37 -39.37 F2
T39.37 -19.68 39.37 -19.68 F3
T39.37 0     39.37 0     F4
T39.37 19.69 39.37 19.69 F5
T39.37 39.37 39.37 39.37 F6
T39.37 59.06 39.37 59.06 F7
T59.06 -59.05 59.06 -59.05 G1
T59.06 -39.37 59.06 -39.37 G2
T59.06 -19.68 59.06 -19.68 G3
T59.06 0     59.06 0     G4
T59.06 19.69 59.06 19.69 G5
T59.06 39.37 59.06 39.37 G6
T59.06 59.06 59.06 59.06 G7
PAD 0 4 P 0    
-2 11.81 R  
-1 0   R  
0  0   R  
21 15.81 R  

*END*
