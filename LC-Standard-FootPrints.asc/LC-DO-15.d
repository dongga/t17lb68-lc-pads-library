*PADS-LIBRARY-PCB-DECALS-V9*

LC-DO-15         I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-311.09 83.87 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-311.09 133.87 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
-188.98 0    
-157.48 0    
OPEN   2 10 26 -1
157.48 0    
188.98 0    
CLOSED 5 10 26 -1
157.48 -70.87
157.48 70.87
-157.48 70.87
-157.48 -70.87
157.48 -70.87
OPEN   4 10 26 -1
-118.11 70.87
-118.11 -70.87
-125.98 -70.87
-125.98 70.87
OPEN   2 8 19 -1
326.77 -19.68
326.77 19.68
OPEN   2 8 19 -1
307.09 0    
346.46 0    
OPEN   2 8 19 -1
-307.09 -19.68
-307.09 19.68
T250   0     250   0     1
T-250  0     -250  0     2
PAD 1 3 P 47.24
-2 86.61 RF  0   0    86.61 0  
-1 86.61 RF  0   0    86.61 0  
0  86.61 RF  0   0    86.61 0  
PAD 2 3 P 47.24
-2 78.74 R  
-1 78.74 R  
0  78.74 R  

*END*
