*PADS-LIBRARY-PCB-DECALS-V9*

LC-LL-41         I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-141.8 79.93 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-141.8 129.93 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-98.43 -57.09
98.43 -57.09
98.43 57.09
-98.43 57.09
-98.43 -57.09
OPEN   2 10 26 -1
-102.36 66.93
102.36 66.93
OPEN   2 10 26 -1
-102.36 -66.93
102.36 -66.93
OPEN   2 10 26 -1
-51.18 -66.93
-51.18 66.93
OPEN   2 8 19 -1
-137.8 -19.68
-137.8 19.68
OPEN   2 8 19 -1
135.83 3.94 
175.2 3.94 
OPEN   2 8 19 -1
155.51 -15.75
155.51 23.62
T94.49 0     94.49 0     1
T-94.49 0     -94.49 0     2
PAD 0 3 P 0    
-2 59.06 RF  0   90   110.24 0  
-1 0   R  
0  0   R  

*END*
