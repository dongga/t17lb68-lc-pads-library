*PADS-LIBRARY-PCB-DECALS-V9*

LC-TSSOP-30      I 0     0     2 2 5 0 30 16 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-177.17 151.73 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-177.17 201.73 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-159.45 -125.98
-171.26 -125.98
CIRCLE 2 15.75 26 -1
-122.05 -47.24
-137.8 -47.24
CIRCLE 2 3.94 18 -1
-100.39 -53.15
-139.76 -53.15
CLOSED 5 10 26 -1
155.51 -70.87
155.51 70.87
-155.51 70.87
-155.51 -70.87
155.51 -70.87
CLOSED 5 3.94 18 -1
155.51 -88.58
155.51 88.58
-155.51 88.58
-155.51 -88.58
155.51 -88.58
T-137.8 -116.14 -137.8 -116.14 1
T-118.11 -116.14 -118.11 -116.14 2
T-98.43 -116.14 -98.43 -116.14 3
T-78.74 -116.14 -78.74 -116.14 4
T-59.05 -116.14 -59.05 -116.14 5
T-39.37 -116.14 -39.37 -116.14 6
T-19.68 -116.14 -19.68 -116.14 7
T0     -116.14 0     -116.14 8
T19.69 -116.14 19.69 -116.14 9
T39.37 -116.14 39.37 -116.14 10
T59.06 -116.14 59.06 -116.14 11
T78.74 -116.14 78.74 -116.14 12
T98.43 -116.14 98.43 -116.14 13
T118.11 -116.14 118.11 -116.14 14
T137.8 -116.14 137.8 -116.14 15
T137.8 116.14 137.8 116.14 16
T118.11 116.14 118.11 116.14 17
T98.43 116.14 98.43 116.14 18
T78.74 116.14 78.74 116.14 19
T59.06 116.14 59.06 116.14 20
T39.37 116.14 39.37 116.14 21
T19.69 116.14 19.69 116.14 22
T0     116.14 0     116.14 23
T-19.68 116.14 -19.68 116.14 24
T-39.37 116.14 -39.37 116.14 25
T-59.05 116.14 -59.05 116.14 26
T-78.74 116.14 -78.74 116.14 27
T-98.43 116.14 -98.43 116.14 28
T-118.11 116.14 -118.11 116.14 29
T-137.8 116.14 -137.8 116.14 30
PAD 0 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 16 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 17 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 18 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 19 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 20 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 21 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 22 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 23 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 24 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 25 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 26 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 27 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 28 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 29 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  
PAD 30 4 P 0    
-2 11.02 OF  90   51.18 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   55.18 0  

*END*
