*PADS-LIBRARY-PCB-DECALS-V9*

LC-LFCSP-16_4X4X05P I 0     0     2 2 7 0 18 11 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-96.49 104.49 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-96.49 154.49 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 9.84 26 -1
-75.79 -105.31
-85.63 -105.31
CIRCLE 2 3.94 18 -1
-43.31 -59.06
-74.8 -59.06
OPEN   3 7.87 26 -1
-59.06 -86.61
-86.61 -86.61
-86.61 -59.06
OPEN   3 7.87 26 -1
59.06 -86.61
86.61 -86.61
86.61 -59.06
OPEN   3 7.87 26 -1
59.06 86.61
86.61 86.61
86.61 59.06
OPEN   3 7.87 26 -1
-59.06 86.61
-86.61 86.61
-86.61 59.06
CLOSED 5 3.94 18 -1
80.71 -80.71
80.71 80.71
-80.71 80.71
-80.71 -80.71
80.71 -80.71
T0     0     0     0     $$$1
T0     0     0     0     0
T-38.39 -76.77 -38.39 -76.77 1
T-12.8 -76.77 -12.8 -76.77 2
T12.8  -76.77 12.8  -76.77 3
T38.39 -76.77 38.39 -76.77 4
T76.77 -38.39 76.77 -38.39 5
T76.77 -12.8 76.77 -12.8 6
T76.77 12.8  76.77 12.8  7
T76.77 38.39 76.77 38.39 8
T38.39 76.77 38.39 76.77 9
T12.8  76.77 12.8  76.77 10
T-12.8 76.77 -12.8 76.77 11
T-38.39 76.77 -38.39 76.77 12
T-76.77 38.39 -76.77 38.39 13
T-76.77 12.8  -76.77 12.8  14
T-76.77 -12.8 -76.77 -12.8 15
T-76.77 -38.39 -76.77 -38.39 16
PAD 1 4 P 0    
-2 98.43 RF  0   90   98.43 0  
-1 0   R  
0  0   R  
21 102.43 RF  0   90   102.43 0  
PAD 2 5 P 0    
-2 98.43 RF  0   90   98.43 0  
-1 0   R  
0  0   R  
21 102.43 RF  0   90   102.43 0  
23 1.97 RF  0   90   1.97 0  
PAD 0 4 P 0    
-2 13.78 OF  90   35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  90   39.43 0  
PAD 7 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  
PAD 8 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  
PAD 9 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  
PAD 10 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  
PAD 15 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  
PAD 16 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  
PAD 17 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  
PAD 18 4 P 0    
-2 13.78 OF  0    35.43 0  
-1 0   R  
0  0   R  
21 17.78 OF  0    39.43 0  

*END*
