*PADS-LIBRARY-PCB-DECALS-V9*

LC-SC-70(SC-70-3) I 0     0     2 2 4 0 3 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V2
-57.87 103.28 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-57.87 53.28 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
CLOSED 5 3.94 18 -1
27.56 -43.31
27.56 43.31
-27.56 43.31
-27.56 -43.31
27.56 -43.31
OPEN   2 7.87 26 -1
35.43 -3.94
35.43 3.94 
OPEN   3 7.87 26 -1
-23.62 -23.62
-23.62 -39.37
7.87  -39.37
OPEN   3 7.87 26 -1
-23.62 23.62
-23.62 39.37
7.87  39.37
T38.19 -25.59 38.19 -25.59 1
T38.19 25.59 38.19 25.59 2
T-38.19 0     -38.19 0     3
PAD 0 3 P 0    
-2 23.62 RF  0   0    39.37 0  
-1 0   R  
0  0   R  

*END*
