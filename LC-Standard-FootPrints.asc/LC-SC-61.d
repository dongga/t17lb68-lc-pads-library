*PADS-LIBRARY-PCB-DECALS-V9*

LC-SC-61         I 0     0     2 2 4 0 4 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-64.69 84.77 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-64.69 134.77 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
60.04 64.96
48.23 64.96
OPEN   2 10 26 -1
-27.56 66.93
27.56 66.93
OPEN   2 10 26 -1
-27.56 -66.93
27.56 -66.93
CLOSED 5 3.94 18 -1
-30   59.06
30    59.06
30    -59.06
-30   -59.06
-30   59.06
T-45   37.5  -45   37.5  1
T-45   -37.3 -45   -37.3 2
T45    -37.5 45    -37.5 3
T45    29.53 45    29.53 4
PAD 0 3 P 0    
-2 19.68 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
PAD 3 3 P 0    
-2 19.68 RF  0   0    51.18 0  
-1 0   R  
0  0   R  
PAD 4 3 P 0    
-2 27.56 RF  0   0    51.18 0  
-1 0   R  
0  0   R  

*END*
