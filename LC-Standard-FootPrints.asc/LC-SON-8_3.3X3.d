*PADS-LIBRARY-PCB-DECALS-V9*

LC-SON-8_3.3X3.3MM I 0     0     2 2 4 0 9 6 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-69.06 85.59 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-69.06 135.59 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 10 26 -1
-54.06 -90.55
-64.06 -90.55
OPEN   2 10 26 -1
-62.01 -59.84
-62.01 58.27
OPEN   2 10 26 -1
62.01 -59.84
62.01 58.27
CLOSED 5 3 18 -1
-65.16 64.96
65.16 64.96
65.16 -64.96
-65.16 -64.96
-65.16 64.96
T-38.39 -59.84 -38.39 -59.84 1
T-12.8 -59.84 -12.8 -59.84 2
T12.8  -59.84 12.8  -59.84 3
T38.39 -59.84 38.39 -59.84 4
T38.39 59.84 38.39 59.84 5
T12.8  59.84 12.8  59.84 6
T-12.8 59.84 -12.8 59.84 7
T0     15.35 0     15.35 8
T-38.39 59.84 -38.39 59.84 8/2
PAD 0 4 P 0    
-2 17.32 RF  0   90   31.5 0  
-1 0   R  
0  0   R  
21 21.32 RF  0   90   35.5 0  
PAD 5 4 P 0    
-2 17.32 RF  0   90   31.5 0  
-1 0   R  
0  0   R  
21 21.32 RF  0   90   35.5 0  
PAD 6 4 P 0    
-2 17.32 RF  0   90   31.5 0  
-1 0   R  
0  0   R  
21 21.32 RF  0   90   35.5 0  
PAD 7 4 P 0    
-2 17.32 RF  0   90   31.5 0  
-1 0   R  
0  0   R  
21 21.32 RF  0   90   35.5 0  
PAD 8 4 P 0    
-2 74.8 RF  0   0    94.09 0  
-1 0   R  
0  0   R  
21 78.8 RF  0   0    98.09 0  
PAD 9 4 P 0    
-2 17.32 RF  0   90   31.5 0  
-1 0   R  
0  0   R  
21 21.32 RF  0   90   35.5 0  

*END*
