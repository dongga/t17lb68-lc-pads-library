*PADS-LIBRARY-PCB-DECALS-V9*

LC-WCSP-5        I 0     0     2 2 5 0 5 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-27.17 27.88 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-27.17 77.88 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 6 26 -1
-17.67 -27.56
-23.67 -27.56
OPEN   3 1.97 18 -1
-9.84 -18.9
-9.84 -5.91
-26.18 -5.91
CLOSED 5 1.97 18 -1
-26.18 -18.9
26.18 -18.9
26.18 18.9 
-26.18 18.9 
-26.18 -18.9
OPEN   2 6 26 -1
-7.87 -15.75
7.87  -15.75
OPEN   2 6 26 -1
-7.87 15.75
7.87  15.75
T-17.05 -9.84 -17.05 -9.84 A1
T-17.05 9.84  -17.05 9.84  A3
T0     0     0     0     B2
T17.05 -9.84 17.05 -9.84 C1
T17.05 9.84  17.05 9.84  C3
PAD 0 4 P 0    
-2 9.84 R  
-1 0   R  
0  0   R  
21 13.84 R  

*END*
