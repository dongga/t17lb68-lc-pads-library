*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-39         I 0     0     2 2 4 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-192.01 200.01 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-192.01 250.01 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 10 26 -1
187.01 0    
-187.01 0    
CIRCLE 2 10 18 -1
187.01 0    
-187.01 0    
OPEN   4 10 26 -1
-142.91 120.47
-164.17 142.13
-142.13 164.17
-120.47 142.91
OPEN   4 10 18 -1
-120.47 142.91
-142.13 164.17
-164.17 142.13
-142.91 120.47
T-98.43 0     -98.43 0     1
T0     -98.43 0     -98.43 2
T98.43 0     98.43 0     3
PAD 0 5 P 27.56
-2 59.06 R  
-1 59.06 R  
0  59.06 R  
21 63.06 R  
28 63.06 R  
PAD 3 5 P 27.56
-2 59.06 RF  0   90   59.06 0  
-1 59.06 RF  0   90   59.06 0  
0  59.06 RF  0   90   59.06 0  
21 63.06 RF  0   90   63.06 0  
28 63.06 RF  0   90   63.06 0  

*END*
