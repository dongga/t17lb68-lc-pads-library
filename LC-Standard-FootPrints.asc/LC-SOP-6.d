*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOP-6         I 0     0     2 2 4 0 6 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-102.36 258   0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-102.36 308   0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-82.68 -226.38
-94.49 -226.38
CIRCLE 2 19.68 26 -1
-37.4 -98.43
-57.09 -98.43
CLOSED 5 10 18 -1
-86.61 -133.86
86.61 -133.86
86.61 133.86
-86.61 133.86
-86.61 -133.86
CLOSED 5 10 26 -1
-78.74 -129.92
78.74 -129.92
78.74 129.92
-78.74 129.92
-78.74 -129.92
T-50   -210.63 -50   -210.63 1
T0     -210.63 0     -210.63 2
T50    -210.63 50    -210.63 3
T50    210.63 50    210.63 4
T0     210.63 0     210.63 5
T-50   210.63 -50   210.63 6
PAD 0 3 P 0    
-2 29.92 OF  90   78.74 0  
-1 0   R  
0  0   R  

*END*
