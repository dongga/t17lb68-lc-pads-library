*PADS-LIBRARY-PCB-DECALS-V9*

LC-SSOP-44K      I 0     0     2 2 4 0 45 24 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-310.93 177.29 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-310.93 227.29 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 7 10 26 -1
-291.34 -19.68 2700 1800 -311.02 -19.68 -271.65 19.68
-291.34 19.68
-291.34 86.61
291.34 86.61
291.34 -86.61
-291.34 -86.61
-291.34 -19.68
CIRCLE 2 11.81 26 -1
-293.21 -148.23
-305.02 -148.23
CIRCLE 2 3.94 18 -1
-244.09 -78.74
-283.46 -78.74
CLOSED 5 3.94 18 -1
-295.28 -110.24
295.28 -110.24
295.28 110.24
-295.28 110.24
-295.28 -110.24
T0     0     0     0     0
T-268.7 -137.8 -268.7 -137.8 1
T-243.11 -137.8 -243.11 -137.8 2
T-217.52 -137.8 -217.52 -137.8 3
T-191.93 -137.8 -191.93 -137.8 4
T-166.34 -137.8 -166.34 -137.8 5
T-140.75 -137.8 -140.75 -137.8 6
T-115.16 -137.8 -115.16 -137.8 7
T-89.57 -137.8 -89.57 -137.8 8
T-63.98 -137.8 -63.98 -137.8 9
T-38.39 -137.8 -38.39 -137.8 10
T-12.8 -137.8 -12.8 -137.8 11
T12.8  -137.8 12.8  -137.8 12
T38.39 -137.8 38.39 -137.8 13
T63.98 -137.8 63.98 -137.8 14
T89.57 -137.8 89.57 -137.8 15
T115.16 -137.8 115.16 -137.8 16
T140.75 -137.8 140.75 -137.8 17
T166.34 -137.8 166.34 -137.8 18
T191.93 -137.8 191.93 -137.8 19
T217.52 -137.8 217.52 -137.8 20
T243.11 -137.8 243.11 -137.8 21
T268.7 -137.8 268.7 -137.8 22
T268.7 137.8 268.7 137.8 23
T243.11 137.8 243.11 137.8 24
T217.52 137.8 217.52 137.8 25
T191.93 137.8 191.93 137.8 26
T166.34 137.8 166.34 137.8 27
T140.75 137.8 140.75 137.8 28
T115.16 137.8 115.16 137.8 29
T89.57 137.8 89.57 137.8 30
T63.98 137.8 63.98 137.8 31
T38.39 137.8 38.39 137.8 32
T12.8  137.8 12.8  137.8 33
T-12.8 137.8 -12.8 137.8 34
T-38.39 137.8 -38.39 137.8 35
T-63.98 137.8 -63.98 137.8 36
T-89.57 137.8 -89.57 137.8 37
T-115.16 137.8 -115.16 137.8 38
T-140.75 137.8 -140.75 137.8 39
T-166.34 137.8 -166.34 137.8 40
T-191.93 137.8 -191.93 137.8 41
T-217.52 137.8 -217.52 137.8 42
T-243.11 137.8 -243.11 137.8 43
T-268.7 137.8 -268.7 137.8 44
PAD 1 3 P 0    
-2 145.67 RF  0   0    196.85 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 24 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 25 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 26 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 27 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 28 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 29 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 30 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 31 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 32 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 33 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 34 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 35 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 36 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 37 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 38 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 39 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 40 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 41 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 42 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 43 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 44 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  
PAD 45 3 P 0    
-2 13.39 OF  90   62.99 0  
-1 0   R  
0  0   R  

*END*
