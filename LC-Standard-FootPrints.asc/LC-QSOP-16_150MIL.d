*PADS-LIBRARY-PCB-DECALS-V9*

LC-QSOP-16_150MIL I 0     0     2 2 5 0 16 9 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-125  145.8 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-125  195.8 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-55.12 -43.31
-70.87 -43.31
CIRCLE 2 11.81 26 -1
-107.28 -119.09
-119.09 -119.09
CIRCLE 2 3.94 18 -1
-55.12 -55.12
-94.49 -55.12
CLOSED 5 3.94 18 -1
-98.43 -78.74
98.43 -78.74
98.43 78.74
-98.43 78.74
-98.43 -78.74
CLOSED 5 10 26 -1
-90.55 -70.87
-90.55 70.87
90.55 70.87
90.55 -70.87
-90.55 -70.87
T-88.19 -114.17 -88.19 -114.17 1
T-62.99 -114.17 -62.99 -114.17 2
T-37.8 -114.17 -37.8 -114.17 3
T-12.6 -114.17 -12.6 -114.17 4
T12.6  -114.17 12.6  -114.17 5
T37.8  -114.17 37.8  -114.17 6
T62.99 -114.17 62.99 -114.17 7
T88.19 -114.17 88.19 -114.17 8
T88.19 114.17 88.19 114.17 9
T62.99 114.17 62.99 114.17 10
T37.8  114.17 37.8  114.17 11
T12.6  114.17 12.6  114.17 12
T-12.6 114.17 -12.6 114.17 13
T-37.8 114.17 -37.8 114.17 14
T-62.99 114.17 -62.99 114.17 15
T-88.19 114.17 -88.19 114.17 16
PAD 0 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 9 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 10 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 11 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 12 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 13 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 14 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 15 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  
PAD 16 3 P 0    
-2 12.6 OF  90   47.24 0  
-1 0   R  
0  0   R  

*END*
