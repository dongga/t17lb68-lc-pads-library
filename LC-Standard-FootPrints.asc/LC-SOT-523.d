*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-523       I 0     0     2 2 2 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-40.35 39.56 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-40.35 89.56 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   3 8 26 -1
-16.73 19.68
-16.73 27.56
2.95  27.56
OPEN   3 8 26 -1
-16.73 -19.68
-16.73 -27.56
2.95  -27.56
T26.57 -19.68 26.57 -19.68 1
T26.57 19.69 26.57 19.69 2
T-26.57 0     -26.57 0     3
PAD 0 3 P 0    
-2 15.75 RF  0   0    23.62 0  
-1 0   R  
0  0   R  
PAD 3 3 P 0    
-2 15.75 RF  0   0    27.56 0  
-1 0   R  
0  0   R  

*END*
