*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMD-4         I 0     0     2 2 4 0 4 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-222.44 133.98 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-222.44 183.98 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 27.56 26 -1
-64.96 49.21
-92.52 49.21
CIRCLE 2 15.75 26 -1
-196.85 110.24
-212.6 110.24
CLOSED 5 3.94 18 -1
-125.98 -94.49
-125.98 94.49
125.98 94.49
125.98 -94.49
-125.98 -94.49
CLOSED 5 10 26 -1
118.11 -86.61
118.11 86.61
-118.11 86.61
-118.11 -86.61
118.11 -86.61
T-183.07 50    -183.07 50    1
T-183.07 -50   -183.07 -50   2
T183.07 -50   183.07 -50   3
T183.07 50    183.07 50    4
PAD 0 3 P 0    
-2 59.06 RF  0   0    78.74 0  
-1 0   R  
0  0   R  

*END*
