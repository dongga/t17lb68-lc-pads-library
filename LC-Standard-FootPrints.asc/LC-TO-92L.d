*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-92L        I 0     0     2 2 2 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-104.33 61.15 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-104.33 111.15 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 7 3.94 18 -1
-74.8 51.18
74.8  51.18
102.36 11.81
102.36 -51.18
-102.36 -51.18
-102.36 11.81
-74.8 51.18
CLOSED 7 7.87 26 -1
-98.43 -47.24
-98.43 11.81
-70.87 47.24
70.87 47.24
98.43 11.81
98.43 -47.24
-98.43 -47.24
T-50   0     -50   0     1
T0     0     0     0     2
T50    0     50    0     3
PAD 0 5 P 27.56
-2 39.37 OF  90   59.06 0  
-1 39.37 OF  90   59.06 0  
0  39.37 OF  90   59.06 0  
21 43.37 OF  90   63.06 0  
28 43.37 OF  90   63.06 0  
PAD 2 5 P 27.56
-2 39.37 OF  90   59.06 0  
-1 39.37 OF  90   59.06 0  
0  39.37 OF  90   59.06 0  
21 43.37 OF  90   63.06 0  
28 43.37 OF  90   63.06 0  

*END*
