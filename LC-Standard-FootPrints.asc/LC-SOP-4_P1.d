*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOP-4_P1.27   I 0     0     2 2 4 0 4 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-157.48 76.9  0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-157.48 126.9 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-127.95 57.09
-139.76 57.09
CIRCLE 2 15.75 26 -1
-30.51 28.54
-46.26 28.54
CLOSED 5 3.94 18 -1
-86.61 -59.06
-86.61 59.06
86.61 59.06
86.61 -59.06
-86.61 -59.06
CLOSED 5 10 26 -1
-62.99 -53.15
-62.99 53.15
62.99 53.15
62.99 -53.15
-62.99 -53.15
T-118.11 25    -118.11 25    1
T-118.11 -25   -118.11 -25   2
T118.11 -25   118.11 -25   3
T118.11 25    118.11 25    4
PAD 0 3 P 0    
-2 23.62 OF  0    78.74 0  
-1 0   R  
0  0   R  

*END*
