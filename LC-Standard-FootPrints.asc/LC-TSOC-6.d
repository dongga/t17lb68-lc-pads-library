*PADS-LIBRARY-PCB-DECALS-V9*

LC-TSOC-6        I 0     0     2 2 4 0 6 4 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-98.43 88.71 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-98.43 138.71 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 3.94 18 -1
47.24 -51.18
15.75 -51.18
CIRCLE 2 11.81 26 -1
92.52 -82.68
80.71 -82.68
CLOSED 5 3.94 18 -1
-57.09 -78.74
57.09 -78.74
57.09 78.74
-57.09 78.74
-57.09 -78.74
CLOSED 5 10 26 -1
19.68 -70.87
19.68 70.87
-19.68 70.87
-19.68 -70.87
19.68 -70.87
T68.9  -50   68.9  -50   1
T68.9  0     68.9  0     2
T68.9  50    68.9  50    3
T-68.9 50    -68.9 50    4
T-68.9 0     -68.9 0     5
T-68.9 -50   -68.9 -50   6
PAD 0 3 P 0    
-2 23.62 RF  0   0    59.06 0  
-1 0   R  
0  0   R  
PAD 4 3 P 0    
-2 23.62 RF  0   0    59.06 0  
-1 0   R  
0  0   R  
PAD 5 3 P 0    
-2 23.62 RF  0   0    59.06 0  
-1 0   R  
0  0   R  
PAD 6 3 P 0    
-2 23.62 RF  0   0    59.06 0  
-1 0   R  
0  0   R  

*END*
