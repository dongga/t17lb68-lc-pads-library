*PADS-LIBRARY-PCB-DECALS-V9*

LC-MQFP-44_10X10X08P I 0     0     2 2 5 0 44 23 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-280.51 288.51 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-280.51 338.51 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 23.62 26 -1
-135.83 -147.64
-159.45 -147.64
CIRCLE 2 11.81 26 -1
-182.09 -258.86
-193.9 -258.86
CIRCLE 2 3.94 18 -1
-141.73 -161.42
-181.1 -161.42
CLOSED 5 10 26 -1
187.01 -187.01
187.01 187.01
-187.01 187.01
-187.01 -187.01
187.01 -187.01
CLOSED 5 3.94 18 -1
196.85 -196.85
196.85 196.85
-196.85 196.85
-196.85 -196.85
196.85 -196.85
T-157.48 -242.13 -157.48 -242.13 1
T-125.98 -242.13 -125.98 -242.13 2
T-94.49 -242.13 -94.49 -242.13 3
T-62.99 -242.13 -62.99 -242.13 4
T-31.5 -242.13 -31.5 -242.13 5
T0     -242.13 0     -242.13 6
T31.5  -242.13 31.5  -242.13 7
T62.99 -242.13 62.99 -242.13 8
T94.49 -242.13 94.49 -242.13 9
T125.98 -242.13 125.98 -242.13 10
T157.48 -242.13 157.48 -242.13 11
T242.13 -157.48 242.13 -157.48 12
T242.13 -125.98 242.13 -125.98 13
T242.13 -94.49 242.13 -94.49 14
T242.13 -62.99 242.13 -62.99 15
T242.13 -31.5 242.13 -31.5 16
T242.13 0     242.13 0     17
T242.13 31.5  242.13 31.5  18
T242.13 62.99 242.13 62.99 19
T242.13 94.49 242.13 94.49 20
T242.13 125.98 242.13 125.98 21
T242.13 157.48 242.13 157.48 22
T157.48 242.13 157.48 242.13 23
T125.98 242.13 125.98 242.13 24
T94.49 242.13 94.49 242.13 25
T62.99 242.13 62.99 242.13 26
T31.5  242.13 31.5  242.13 27
T0     242.13 0     242.13 28
T-31.5 242.13 -31.5 242.13 29
T-62.99 242.13 -62.99 242.13 30
T-94.49 242.13 -94.49 242.13 31
T-125.98 242.13 -125.98 242.13 32
T-157.48 242.13 -157.48 242.13 33
T-242.13 157.48 -242.13 157.48 34
T-242.13 125.98 -242.13 125.98 35
T-242.13 94.49 -242.13 94.49 36
T-242.13 62.99 -242.13 62.99 37
T-242.13 31.5  -242.13 31.5  38
T-242.13 0     -242.13 0     39
T-242.13 -31.5 -242.13 -31.5 40
T-242.13 -62.99 -242.13 -62.99 41
T-242.13 -94.49 -242.13 -94.49 42
T-242.13 -125.98 -242.13 -125.98 43
T-242.13 -157.48 -242.13 -157.48 44
PAD 0 3 P 0    
-2 19.29 OF  90   76.77 0  
-1 0   R  
0  0   R  
PAD 12 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 13 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 14 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 15 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 16 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 17 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 18 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 19 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 20 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 21 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 22 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 34 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 35 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 36 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 37 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 38 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 39 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 40 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 41 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 42 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 43 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  
PAD 44 3 P 0    
-2 19.29 OF  0    76.77 0  
-1 0   R  
0  0   R  

*END*
